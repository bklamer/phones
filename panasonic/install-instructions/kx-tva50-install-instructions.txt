1. Plug in the TVA50 through the usb cable
2. install the usb drivers
    1. Install the drivers through device manager
    2. If you need to disable driver signing checks
        1. Hold Shift Key and click on Restart
        2. Click on Troubleshoot
        3. Click on Advanced options
        4. Click on Startup Settings
        5. Click on Restart
        6. Select the 7th option
3. Open the TVA50 ISO image 
    1. Install the maintenance console from the software folder
4. Open the maintenance console.
    1. The programmers code is `INSTALLER`
